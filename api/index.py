#!/usr/bin/env python3

import os

import boto3
from flask import Flask
from flask import redirect
from flask import render_template
from flask import request

import aurora_data_api

from flask_sqlalchemy import SQLAlchemy

cluster_arn = os.environ.get('CLUSTER_ARN')
secret_arn = os.environ.get('SECRET_ARN')
db_name = os.environ.get('DB_NAME')
region = os.environ.get('REGION')

access_key = os.environ.get('ACCESS_KEY')
secret_key = os.environ.get('SECRET_KEY')


# Temporary Monkey Patch until library gets updated to fix limitation, see PR created:
# https://github.com/chanzuckerberg/aurora-data-api/pull/10
def monkey_connect(aurora_cluster_arn=None, secret_arn=None, database=None, rds_data_client=None, host=None,
                   username=None, password=None, charset=None):
    return aurora_data_api.AuroraDataAPIClient(dbname=database, aurora_cluster_arn=aurora_cluster_arn,
                                               secret_arn=secret_arn, rds_data_client=rds_data_client, charset=charset)


aurora_data_api.connect = monkey_connect

rds_data_client = boto3.client(
    'rds-data',
    aws_access_key_id=access_key,
    aws_secret_access_key=secret_key,
    region_name=region
)

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql+auroradataapi://:@/{db_name}'
app.config['SQLALCHEMY_ENGINE_OPTIONS'] = dict(
    echo=True,
    connect_args=dict(aurora_cluster_arn=cluster_arn, secret_arn=secret_arn, rds_data_client=rds_data_client)
)

db = SQLAlchemy(app)


class Book(db.Model):
    title = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)

    def __repr__(self):
        return '<Title: {}>'.format(self.title)


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.form:
        try:
            book = Book(title=request.form.get('title'))
            db.session.add(book)
            db.session.commit()
        except Exception as e:
            print('Failed to add book')
            print(e)
    books = Book.query.all()
    return render_template('home.html', books=books)


@app.route('/update', methods=['POST'])
def update():
    try:
        newtitle = request.form.get('newtitle')
        oldtitle = request.form.get('oldtitle')
        book = Book.query.filter_by(title=oldtitle).first()
        book.title = newtitle
        db.session.commit()
    except Exception as e:
        print('Couldn\'t update book title')
        print(e)
    return redirect('/')


@app.route('/delete', methods=['POST'])
def delete():
    title = request.form.get('title')
    book = Book.query.filter_by(title=title).first()
    db.session.delete(book)
    db.session.commit()
    return redirect('/')


if __name__ == '__main__':
    app.run(debug=True)
